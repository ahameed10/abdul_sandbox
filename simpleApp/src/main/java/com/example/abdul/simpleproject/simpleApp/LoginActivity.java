package com.example.abdul.simpleproject.simpleApp;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String USERNAME = "username";
    private static final String PASSWORD = "password";
    private TextView tvError;
    private EditText etUsername;
    private EditText etPassword;
    private Button btnSignin;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        context = this;

        tvError = (TextView) this.findViewById(R.id.tv_login_error);
        etUsername = (EditText) this.findViewById(R.id.et_username);
        etPassword = (EditText) this.findViewById(R.id.et_password);
        btnSignin = (Button) this.findViewById(R.id.btn_signin);
        btnSignin.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_signin:
                final String user = etUsername.getText().toString();
                final String pass = (String) etPassword.getText().toString();

                if (!user.equalsIgnoreCase(USERNAME) || !pass.equalsIgnoreCase(PASSWORD)) {
                    tvError.setVisibility(View.VISIBLE);
                    return;
                }
                btnSignin.setText(R.string.username);
                break;
        }

    }
}
